package br.com.challege.builders.clients.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import br.com.challege.builders.clients.entity.Client;

/**
 * 
 * @author pedro.carvalho
 * @date 26/09/2020
 * 
 */

@RepositoryRestResource(path = "client", collectionResourceRel = "clients")
public interface ClientRepository extends JpaRepository<Client, Long>, JpaSpecificationExecutor<Client> {
	
}