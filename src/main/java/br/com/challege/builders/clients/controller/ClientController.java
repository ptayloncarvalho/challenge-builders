package br.com.challege.builders.clients.controller;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.com.challege.builders.clients.entity.Client;
import br.com.challege.builders.clients.entity.ClientDTO;
import br.com.challege.builders.clients.repository.ClientRepository;
import br.com.challege.builders.clients.specification.ClientSpecification;

/**
 * 
 * @author pedro.carvalho
 * @date 26/09/2020
 * 
 */

@RestController
@RequestMapping(value="/api/client")
public class ClientController extends AbstractRestController<Client, ClientDTO, Long, ClientRepository> {
	
	@GetMapping(value="/findClientByCpfOrName")
	public Page<Client> findClients(Pageable pageable,
						    @RequestParam(value = "cpf", required=false) String cpf,
						    @RequestParam(value = "client_name", required=false) String clientName) {
		
		return getRepo().findAll(
				new ClientSpecification(new Client(clientName, cpf)
			), pageable
		);
		
	}

} 