package br.com.challege.builders.clients.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * 
 * @author pedro.carvalho
 * @date 26/09/2020
 * 
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
@Entity
@Table(name = "client")
public class Client {
	
	public Client(String name, String cpf) {
		this.name = name;
		this.cpf = cpf;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name="cpf")
	private String cpf;

	@Column(name="client_name")
	private String name;

	@Column(name = "date_of_birth", columnDefinition = "DATE")
	private Date dateOfBirth;
	
}