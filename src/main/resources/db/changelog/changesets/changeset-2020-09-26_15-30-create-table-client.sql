--liquibase formatted sql

--changeset pedro.carvalho:2020-09-26_15-30-create-table-client endDelimiter:/ rollbackEndDelimiter:/

CREATE TABLE CLIENT (  
    ID BIGSERIAL NOT NULL,
    CLIENT_NAME VARCHAR(200), 
    CPF VARCHAR(22) UNIQUE, 
    DATE_OF_BIRTH DATE,
    PRIMARY KEY (ID)
)
/
 
--rollback DROP TABLE CLIENT; /