--liquibase formatted sql

--changeset pedro.carvalho:2020-09-26_15-31-load-seed-data endDelimiter:/ rollbackEndDelimiter:/


INSERT INTO client(client_name, cpf, date_of_birth)
VALUES
(
'Colin Oliver', '782.344.734-76', '1998-3-28'
)/

INSERT INTO client(client_name, cpf, date_of_birth)
VALUES
(
'Calvin Witt', '645.242.771-70', '2019-9-13'
)/

INSERT INTO client(client_name, cpf, date_of_birth)
VALUES
(
'Martin Sandoval', '867.507.431-02', '1986-8-01'
)/

INSERT INTO client(client_name, cpf, date_of_birth)
VALUES
(
'Jasper Webb', '876.456.571-89', '1993-2-24'
)/

INSERT INTO client(client_name, cpf, date_of_birth)
VALUES
(
'Cade Schneider', '752.024.528-45', '1990-4-30'
)/

INSERT INTO client(client_name, cpf, date_of_birth)
VALUES
(
'Stephen Wooten', '604.581.422-39', '1974-12-31'
)/

INSERT INTO client(client_name, cpf, date_of_birth)
VALUES
(
'Brent Valencia', '063.161.785-06', '1993-1-02'
)/

INSERT INTO client(client_name, cpf, date_of_birth)
VALUES
(
'Avram Hopkins', '221.186.486-42', '1992-11-04'
)/

INSERT INTO client(client_name, cpf, date_of_birth)
VALUES
(
'Ryder Clemons', '836.606.153-15', '1999-7-21'
)/

INSERT INTO client(client_name, cpf, date_of_birth)
VALUES
(
'Hop Tillman', '376.571.685-52', '2020-1-03'
)/

INSERT INTO client(client_name, cpf, date_of_birth)
VALUES
(
'Uriah Blanchard', '653.273.172-26', '1975-12-25'
)/

INSERT INTO client(client_name, cpf, date_of_birth)
VALUES
(
'Joshua Arnold', '702.263.833-00', '1989-7-27'
)/

INSERT INTO client(client_name, cpf, date_of_birth)
VALUES
(
'Orson Foley', '356.483.821-03', '1985-12-19'
)/

INSERT INTO client(client_name, cpf, date_of_birth)
VALUES
(
'Zachary Nunez', '161.434.530-98', '2017-1-23'
)/

INSERT INTO client(client_name, cpf, date_of_birth)
VALUES
(
'Ali Harding', '047.205.223-35', '2014-2-05'
)/

INSERT INTO client(client_name, cpf, date_of_birth)
VALUES
(
'Akeem Dorsey', '586.600.256-60', '2006-1-12'
)/

INSERT INTO client(client_name, cpf, date_of_birth)
VALUES
(
'Kelly Terrell', '548.634.567-43', '2019-5-31'
)/

INSERT INTO client(client_name, cpf, date_of_birth)
VALUES
(
'Josiah Williams', '722.824.435-43', '1982-3-12'
)/

INSERT INTO client(client_name, cpf, date_of_birth)
VALUES
(
'Jamal Hampton', '155.405.843-09', '1991-9-24'
)/

INSERT INTO client(client_name, cpf, date_of_birth)
VALUES
(
'Neville Ramsey', '068.602.453-28', '1996-1-03'
)/

INSERT INTO client(client_name, cpf, date_of_birth)
VALUES
(
'Holmes Blanchard', '880.473.443-40', '2006-11-30'
)/

INSERT INTO client(client_name, cpf, date_of_birth)
VALUES
(
'Clarke Vincent', '066.571.108-55', '2016-11-06'
)/

INSERT INTO client(client_name, cpf, date_of_birth)
VALUES
(
'Alvin Hurley', '473.878.526-50', '1983-6-05'
)/

INSERT INTO client(client_name, cpf, date_of_birth)
VALUES
(
'Gregory Gross', '742.010.067-51', '1975-4-19'
)/

INSERT INTO client(client_name, cpf, date_of_birth)
VALUES
(
'Hasad Woodward', '110.516.043-20', '2016-3-29'
)/

INSERT INTO client(client_name, cpf, date_of_birth)
VALUES
(
'Laith Sullivan', '841.788.054-28', '2009-7-31'
)/

INSERT INTO client(client_name, cpf, date_of_birth)
VALUES
(
'Sylvester Lara', '605.350.452-12', '1990-8-26'
)/

INSERT INTO client(client_name, cpf, date_of_birth)
VALUES
(
'Graham Alexander', '466.077.717-00', '2017-6-20'
)/

INSERT INTO client(client_name, cpf, date_of_birth)
VALUES
(
'Hilel Miles', '422.516.774-70', '2013-5-14'
)/

INSERT INTO client(client_name, cpf, date_of_birth)
VALUES
(
'Clark Steele', '677.047.554-55', '1995-1-20'
)/

INSERT INTO client(client_name, cpf, date_of_birth)
VALUES
(
'Rigel Alexander', '500.524.223-64', '1996-5-27'
)/

INSERT INTO client(client_name, cpf, date_of_birth)
VALUES
(
'Clark Hays', '802.632.682-25', '2008-11-16'
)/

INSERT INTO client(client_name, cpf, date_of_birth)
VALUES
(
'Lawrence Snow', '460.625.420-31', '2003-1-02'
)/

INSERT INTO client(client_name, cpf, date_of_birth)
VALUES
(
'Merrill Campos', '353.782.554-49', '1981-6-14'
)/

INSERT INTO client(client_name, cpf, date_of_birth)
VALUES
(
'Xander Holder', '155.245.486-03', '2017-11-22'
)/

INSERT INTO client(client_name, cpf, date_of_birth)
VALUES
(
'Callum Case', '566.781.733-09', '2013-1-02'
)/

INSERT INTO client(client_name, cpf, date_of_birth)
VALUES
(
'Abraham Blankenship', '373.118.268-88', '1978-8-11'
)/

INSERT INTO client(client_name, cpf, date_of_birth)
VALUES
(
'Talon Oneal', '432.027.541-14', '2011-5-17'
)/

INSERT INTO client(client_name, cpf, date_of_birth)
VALUES
(
'Martin Maxwell', '860.624.171-72', '2001-11-04'
)/

INSERT INTO client(client_name, cpf, date_of_birth)
VALUES
(
'Isaiah Parks', '254.463.752-89', '2021-6-16'
)/

INSERT INTO client(client_name, cpf, date_of_birth)
VALUES
(
'Keegan Gallegos', '587.418.611-59', '1988-10-31'
)/

INSERT INTO client(client_name, cpf, date_of_birth)
VALUES
(
'Armando Watts', '243.347.281-46', '1980-7-10'
)/

INSERT INTO client(client_name, cpf, date_of_birth)
VALUES
(
'Hayes Morrow', '406.621.167-47', '1983-12-16'
)/

INSERT INTO client(client_name, cpf, date_of_birth)
VALUES
(
'Driscoll Conrad', '252.588.330-62', '1996-11-30'
)/

INSERT INTO client(client_name, cpf, date_of_birth)
VALUES
(
'Octavius Garrett', '270.174.426-10', '2002-3-14'
)/

INSERT INTO client(client_name, cpf, date_of_birth)
VALUES
(
'Vance Decker', '225.165.836-05', '1999-4-04'
)/

INSERT INTO client(client_name, cpf, date_of_birth)
VALUES
(
'Arsenio Powers', '033.556.585-96', '2021-6-26'
)/

INSERT INTO client(client_name, cpf, date_of_birth)
VALUES
(
'Jamal Guerra', '426.324.113-43', '2015-3-06'
)/

INSERT INTO client(client_name, cpf, date_of_birth)
VALUES
(
'Christian Watts', '353.165.450-01', '1996-10-25'
)/

INSERT INTO client(client_name, cpf, date_of_birth)
VALUES
(
'Eagan Cardenas', '824.525.861-02', '1974-11-23'
)/

INSERT INTO client(client_name, cpf, date_of_birth)
VALUES
(
'Lyle Blackwell', '421.051.762-32', '2013-8-17'
)/

INSERT INTO client(client_name, cpf, date_of_birth)
VALUES
(
'Drew Pollard', '053.180.474-75', '1989-7-01'
)/

INSERT INTO client(client_name, cpf, date_of_birth)
VALUES
(
'Hunter Banks', '710.803.153-14', '2002-7-08'
)/

INSERT INTO client(client_name, cpf, date_of_birth)
VALUES
(
'Bert Lott', '427.568.452-48', '2012-3-20'
)/

INSERT INTO client(client_name, cpf, date_of_birth)
VALUES
(
'Stuart Young', '638.812.231-00', '2005-11-28'
)/

INSERT INTO client(client_name, cpf, date_of_birth)
VALUES
(
'Tyrone Salas', '057.228.386-59', '1996-7-03'
)/

INSERT INTO client(client_name, cpf, date_of_birth)
VALUES
(
'Laith Wolfe', '472.522.470-74', '2019-1-09'
)/

INSERT INTO client(client_name, cpf, date_of_birth)
VALUES
(
'Caldwell Johns', '315.376.245-71', '1982-12-09'
)/

INSERT INTO client(client_name, cpf, date_of_birth)
VALUES
(
'Dean Roberson', '246.353.853-88', '2000-11-01'
)/

INSERT INTO client(client_name, cpf, date_of_birth)
VALUES
(
'Yasir Calhoun', '582.210.450-98', '2005-10-20'
)/

INSERT INTO client(client_name, cpf, date_of_birth)
VALUES
(
'Cade Buck', '577.518.824-17', '2019-9-22'
)/

INSERT INTO client(client_name, cpf, date_of_birth)
VALUES
(
'Curran Bauer', '328.842.657-91', '1985-7-07'
)/

INSERT INTO client(client_name, cpf, date_of_birth)
VALUES
(
'Noble Jacobson', '753.111.140-37', '1978-2-07'
)/

INSERT INTO client(client_name, cpf, date_of_birth)
VALUES
(
'Rudyard Kelly', '863.821.450-25', '1987-10-01'
)/

INSERT INTO client(client_name, cpf, date_of_birth)
VALUES
(
'Brenden Porter', '442.654.458-02', '1998-5-06'
)/

INSERT INTO client(client_name, cpf, date_of_birth)
VALUES
(
'Dante Noble', '605.258.347-96', '1975-4-26'
)/

INSERT INTO client(client_name, cpf, date_of_birth)
VALUES
(
'Zane Anthony', '252.147.437-18', '2011-11-04'
)/

INSERT INTO client(client_name, cpf, date_of_birth)
VALUES
(
'Marvin Foreman', '672.604.051-20', '2013-10-15'
)/

INSERT INTO client(client_name, cpf, date_of_birth)
VALUES
(
'Kenneth Roth', '404.242.767-71', '1984-4-02'
)/

INSERT INTO client(client_name, cpf, date_of_birth)
VALUES
(
'Hector Tate', '472.287.300-38', '2004-4-03'
)/

INSERT INTO client(client_name, cpf, date_of_birth)
VALUES
(
'Yasir Burnett', '141.688.867-51', '1994-6-08'
)/

INSERT INTO client(client_name, cpf, date_of_birth)
VALUES
(
'Zephania Guzman', '443.361.785-77', '1993-2-19'
)/

INSERT INTO client(client_name, cpf, date_of_birth)
VALUES
(
'Oren Duran', '040.585.724-18', '1997-8-11'
)/

INSERT INTO client(client_name, cpf, date_of_birth)
VALUES
(
'Levi Oconnor', '352.147.286-89', '2009-1-28'
)/

INSERT INTO client(client_name, cpf, date_of_birth)
VALUES
(
'Lawrence Norman', '861.615.723-97', '1982-12-09'
)/

INSERT INTO client(client_name, cpf, date_of_birth)
VALUES
(
'Tad Sellers', '800.275.034-96', '1997-11-27'
)/

INSERT INTO client(client_name, cpf, date_of_birth)
VALUES
(
'August Dominguez', '486.800.212-02', '1973-8-03'
)/

INSERT INTO client(client_name, cpf, date_of_birth)
VALUES
(
'Cairo Roy', '732.228.277-96', '1977-1-01'
)/

INSERT INTO client(client_name, cpf, date_of_birth)
VALUES
(
'Garth Strickland', '750.837.035-05', '1993-3-28'
)/

INSERT INTO client(client_name, cpf, date_of_birth)
VALUES
(
'Dale Gilliam', '326.765.355-01', '1990-4-30'
)/

INSERT INTO client(client_name, cpf, date_of_birth)
VALUES
(
'Stewart Coleman', '156.437.447-59', '2016-11-06'
)/

INSERT INTO client(client_name, cpf, date_of_birth)
VALUES
(
'Ezra Hardy', '165.820.621-58', '2020-8-23'
)/

INSERT INTO client(client_name, cpf, date_of_birth)
VALUES
(
'Keane Quinn', '404.716.483-61', '1981-4-12'
)/

INSERT INTO client(client_name, cpf, date_of_birth)
VALUES
(
'Avram Mayer', '707.321.743-09', '2019-5-12'
)/

INSERT INTO client(client_name, cpf, date_of_birth)
VALUES
(
'Cameron Duran', '387.104.766-01', '1987-8-08'
)/

INSERT INTO client(client_name, cpf, date_of_birth)
VALUES
(
'Xanthus Owens', '736.854.135-11', '1977-6-19'
)/

INSERT INTO client(client_name, cpf, date_of_birth)
VALUES
(
'Cadman Parrish', '673.733.363-02', '1991-7-29'
)/

INSERT INTO client(client_name, cpf, date_of_birth)
VALUES
(
'Driscoll Bean', '527.854.644-19', '2018-6-27'
)/

INSERT INTO client(client_name, cpf, date_of_birth)
VALUES
(
'Keegan Wilcox', '828.372.147-01', '1984-1-12'
)/

INSERT INTO client(client_name, cpf, date_of_birth)
VALUES
(
'Eagan Schneider', '341.016.180-54', '1994-1-20'
)/

INSERT INTO client(client_name, cpf, date_of_birth)
VALUES
(
'Denton Tanner', '414.864.457-48', '2006-6-17'
)/

INSERT INTO client(client_name, cpf, date_of_birth)
VALUES
(
'Jameson Wolfe', '003.283.222-29', '1995-1-25'
)/

INSERT INTO client(client_name, cpf, date_of_birth)
VALUES
(
'Keaton Bryan', '214.158.037-49', '1986-9-24'
)/

INSERT INTO client(client_name, cpf, date_of_birth)
VALUES
(
'Octavius Terry', '545.366.833-48', '2005-10-25'
)/

INSERT INTO client(client_name, cpf, date_of_birth)
VALUES
(
'Salvador Williams', '186.066.383-40', '1977-7-08'
)/

INSERT INTO client(client_name, cpf, date_of_birth)
VALUES
(
'Declan Spears', '413.645.038-91', '2010-8-11'
)/

INSERT INTO client(client_name, cpf, date_of_birth)
VALUES
(
'Theodore Hays', '810.562.118-57', '1982-5-22'
)/

INSERT INTO client(client_name, cpf, date_of_birth)
VALUES
(
'Porter Pace', '771.867.564-50', '2000-7-13'
)/

INSERT INTO client(client_name, cpf, date_of_birth)
VALUES
(
'Gregory Bauer', '073.133.475-29', '2001-10-04'
)/

INSERT INTO client(client_name, cpf, date_of_birth)
VALUES
(
'Cole Coffey', '776.657.613-99', '1986-2-28'
)/

--rollback TRUNCATE CLIENT; /