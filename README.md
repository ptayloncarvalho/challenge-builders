# Platform Builder - Desafio App

Aplicativo desafio Platform Builder, criado utilizando Spring-Boot 2.3.4.RELEASE e maven como gerenciador de dependências.

Para carregar os dados nos testes do `POSTMAN` foi utilizado o módulo `LIQUIBASE` para tratamento de migrations no banco de dados trazendo segurança e gerência de configuração dos scripts SQL.

Desenvolvido também a abstração `AbstractRestController` utilizando `Generics` que é responsável por todas 
as operações GET, POST, DELETE, PATCH e PUT e conversão entre Entidade para DTO, agilizando nas construções de demais Controllers da aplicação.


### Requisitos para rodar na IDE

A versão `JAVA 8` instalada e também `Lombok` configurado em sua IDE (para mais detalhes do projeto 
lombok para sua IDE em https://projectlombok.org).

### Iniciar o Serviço

Para iniciar o serviço execute:

```sh
$ docker-compose up -d
$ mvn spring-boot:run
```

### Postman

O arquivo de importação do `POSTMAN` é o `PlatformBuilders.postman_collection.json` e encontra-se na raiz do projeto.	

### Framework e Dependências

Para do desenvolvimento dessa aplicação foram necessários utilizar as seguintes 
frameworks e dependências.

| Dependência ou Framework | Descrição |
| ------ | ------ |
| Stpring Boot 2.3.4.RELEASE | Base da aplicação |
| Postgres | Banco de dados |
| spring-boot-starter-data-jpa | Para persistência no Banco de Dados |
| spring-boot-starter-data-rest | Para expor as interfaces de serviços REST |
| project-lombok | Para código ágil e limpo |
| spring-boot-devtools | Para depuração e debug de forma ágil |
| model-mapper | Para trafegar dados entre Entidades e DTO's 
| liquibase-core | Para tratamento das migrations no SGBD |
| Docker | Container para nosso Database
